///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Reference time: Tue Jan 21 04:26:07 PM HST 2014
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 57
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 58
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 59
//   Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 0
//   Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 1
//   Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 2
//   Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 3
//
// @author Reid Lum <reidlum@hawaii.edu>
// @date   21 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

int main()
{
   time_t RefSecSinceEpoch;
   struct tm info;
   char buffer[80];

   info.tm_year = 2014 - 1900;
   info.tm_mon = 1 - 1;
   info.tm_mday = 21;
   info.tm_hour = 16;
   info.tm_min = 26;
   info.tm_sec = 7;
   info.tm_isdst = -1;

   RefSecSinceEpoch = mktime(&info);
   if( RefSecSinceEpoch == -1 ) {
      printf("Error: unable to make time using mktime\n");
   } else {
      strftime(buffer, sizeof(buffer), "%a %b %d %I:%M:%S %p %Z %Y\n", &info );
      printf("Reference Time: %s",buffer);
   }
   
   int loop = 0;
   while ( loop < 1 )
   {
       time_t CurrentSecSinceEpoch = time(NULL);
       if (CurrentSecSinceEpoch > RefSecSinceEpoch)
       {
           time_t CurrentSecSinceRef = CurrentSecSinceEpoch-RefSecSinceEpoch;
           int years = CurrentSecSinceRef / (60 * 60 * 24 * 365);
           CurrentSecSinceRef -= years * (60 * 60 * 24 * 365);
           int days = CurrentSecSinceRef / (60 * 60 * 24);
           CurrentSecSinceRef -= days * (60 * 60 * 24);
           int hours = CurrentSecSinceRef / (60 * 60);
           CurrentSecSinceRef -= hours * (60 * 60);
           int minutes = CurrentSecSinceRef / 60;
           CurrentSecSinceRef -= minutes * 60;
           printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %ld\n",years,days,hours,minutes,CurrentSecSinceRef);
           sleep(1);
       }
       else if (CurrentSecSinceEpoch < RefSecSinceEpoch)
       {
           time_t CurrentSecSinceRef = RefSecSinceEpoch-CurrentSecSinceEpoch;
           int years = CurrentSecSinceRef / (60 * 60 * 24 * 365);
           CurrentSecSinceRef -= years * (60 * 60 * 24 * 365);
           int days = CurrentSecSinceRef / (60 * 60 * 24);
           CurrentSecSinceRef -= days * (60 * 60 * 24);
           int hours = CurrentSecSinceRef / (60 * 60);
           CurrentSecSinceRef -= hours * (60 * 60);
           int minutes = CurrentSecSinceRef / 60;
           CurrentSecSinceRef -= minutes * 60;
           printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %ld\n",years,days,hours,minutes,CurrentSecSinceRef);
           sleep(1);
       }
       else
       {
           printf("Years: 0 Days: 0 Hours: 0 Minutes: 0 Seconds: 0\n");
           sleep(1);
       }
   }
   return(0);

}


